package com.example.elementos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import android.widget.Toast.makeText
import android.widget.ToggleButton

class Toggle : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toggle)

        val btnHome: Button = findViewById(R.id.btnHome)
        btnHome.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val tBtn: ToggleButton = findViewById(R.id.togglebtn)
        tBtn.setOnClickListener {
            if(tBtn.isChecked){
                makeText(this, "El toggle esta encendido.", Toast.LENGTH_SHORT).show()
            }else{
                makeText(this, "El toggle esta apagado.", Toast.LENGTH_SHORT).show()
            }
        }
    }
}