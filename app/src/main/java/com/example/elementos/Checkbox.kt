package com.example.elementos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import android.widget.Toast.makeText

class Checkbox : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkbox)

        val btnHome: Button = findViewById(R.id.btnHome)
        btnHome.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val cL: CheckBox = findViewById(R.id.cBLunes)
        val cM: CheckBox = findViewById(R.id.cBMartes)
        val cX: CheckBox = findViewById(R.id.cBMiercoles)
        val cJ: CheckBox = findViewById(R.id.cBJueves)
        val cV: CheckBox = findViewById(R.id.cBViernes)

        val btnVal: Button = findViewById(R.id.btnValidar)
        btnVal.setOnClickListener {
            if (cL.isChecked){
                makeText(this, "Lunes", Toast.LENGTH_SHORT).show()
            }
            if (cM.isChecked){
                makeText(this, "Martes", Toast.LENGTH_SHORT).show()
            }
            if (cX.isChecked){
                makeText(this, "Miercoles", Toast.LENGTH_SHORT).show()
            }
            if (cJ.isChecked){
                makeText(this, "Jueves", Toast.LENGTH_SHORT).show()
            }
            if (cV.isChecked){
                makeText(this, "Viernes", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
