package com.example.elementos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Switch
import android.widget.Toast

class Switch : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_switch)

        val btnHome: Button = findViewById(R.id.btnHome)
        btnHome.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val sWP: Switch = findViewById(R.id.switch3)
        val sA: Switch = findViewById(R.id.switch1)
        val siOS: Switch = findViewById(R.id.switch2)

        sWP.setOnClickListener {
            if(sWP.isChecked){
                Toast.makeText(this, "Windows Phone esta encendido.", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "Windows Phone esta apagado.", Toast.LENGTH_SHORT).show()
            }
        }

        sA.setOnClickListener {
            if(sA.isChecked){
                Toast.makeText(this, "Android esta encendido.", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "Android Phone esta apagado.", Toast.LENGTH_SHORT).show()
            }
        }

        siOS.setOnClickListener {
            if(siOS.isChecked){
                Toast.makeText(this, "iOS esta encendido.", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "iOS esta apagado.", Toast.LENGTH_SHORT).show()
            }
        }
    }
}