package com.example.elementos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.i
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnCB: Button = findViewById(R.id.btnCheckbox)
        btnCB.setOnClickListener {
            val intent = Intent(this, Checkbox::class.java)
            startActivity(intent)
        }

        val btnTg: Button = findViewById(R.id.btnToggle)
        btnTg.setOnClickListener {
            val intent = Intent(this, Toggle::class.java)
            startActivity(intent)
        }

        val btnSw: Button = findViewById(R.id.btnSwitch)
        btnSw.setOnClickListener {
            val intent = Intent(this, Switch::class.java)
            startActivity(intent)
        }
    }
}